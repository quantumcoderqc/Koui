let project = new Project('Tests');

project.addSources(".");
project.addLibrary("utest");
await project.addProject("../");

// For higher build test coverage
project.addDefine("KOUI_DEBUG_LAYOUT");

// project.addDefine("UTEST_PRINT_TESTS");

resolve(project);
