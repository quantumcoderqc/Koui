package koui.events;

import koui.elements.Element;

/**
 * This typedef holds all information about a certain event. Each event has a
 * type and a state and – depending on its type – further attributes, that are
 * marked as `@:optional`.
 *
 * @see [Wiki: Events](https://gitlab.com/koui/Koui/-/wikis/Documentation/Events)
 */
typedef Event = {
	/**
	 * The type of the event.
	 */
	final type: EventType;
	/**
	 * The state of the event.
	 */
	final state: EventState;

	/**
	 * The element that receives this event. Although marked as optional for
	 * internal reasons, this attribute does always exist.
	 */
	@:optional var element: Element;

	/**
	 * `true` if the mouse was moved in the last frame.
	 * @see `EventType.MouseHover`
	 */
	@:optional final mouseMoved: Bool;
	/**
	 * The mouse button that triggered the event.
	 * @see `EventType.Focus`
	 * @see `EventType.MouseClick`
	 */
	@:optional final mouseButton: MouseButton;
	/**
	 * The amount of mouse wheel rotation.
	 * @see `EventType.MouseScroll`
	 */
	@:optional final scrollDelta: Int;
	/**
	 * The key code that triggered this event.
	 * @see `EventType.KeyCodePress`
	 * @see `EventType.KeyCodeStatus`
	 * @see [List of key codes (external link)](http://api.kha.tech/kha/input/KeyCode.html)
	 */
	@:optional final keyCode: kha.input.KeyCode;
	/**
	 * The character representation of the key that triggered this event.
	 * @see `EventType.KeyCharPress`
	 * @see `StringUtil.canPrintChar()`
	 */
	@:optional final keyChar: String;
}

/**
 * Represents an event type.
 *
 * @see [Wiki: Events/Event Types](https://gitlab.com/koui/Koui/-/wikis/Documentation/Events#event-types)
 */
enum abstract EventType(Int) from Int to Int {
	/**
	 * `MouseHover` events are fired if the mouse cursor is above an element.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: fired when the mouse first hovers the element
	 * - `Active`: (every frame when the mouse hovers the element)
	 * - `Deactivated`: (when the mouse is no longer on the element that was
	 *   hovered in the last frame). If an objects gets an `Deactivated` event,
	 *   another element might already receive an `Activated` event.
	 *
	 * **Further attributes:**
	 *
	 * - `mouseMoved`: might be `true` or `false` depending on whether the mouse
	 *   was moved or is stationary over the element.
	 */
	var MouseHover = 1 << 0;

	/**
	 * `MouseClick` events are fired if any mouse button is clicked. Only
	 * hovered or focused elements can receive such events.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: Received by the hovered element if the mouse first clicks
	 *   on it
	 * - `Active`: Received by the focused element if any mouse button is
	 *   pressed
	 * - `Deactivated`: Received by the focused element when the mouse button
	 *   was released and the element is still hovered
	 * - `Cancelled`: The same as `Deactivated` but fired when the focused
	 *   element is no longer hovered
	 *
	 * **Further attributes:**
	 *
	 * - `mouseButton`: A `MouseButton` value describing the mouse
	 *   button that triggered this event.
	 */
	var MouseClick = 1 << 1;

	/**
	 * A `MouseScroll` event is fired when the mouse wheel is rotated. Is it
	 *   sent to the currently hovered element.
	 *
	 * **Possible states:**
	 *
	 *  - `Active`: The only state of this event. Either the mouse is scrolled
	 *   or not.
	 *
	 * **Further attributes:**
	 *
	 * - `scrollDelta: Int`: The amount of mouse wheel rotation. The faster the
	 *   mouse is scrolled, the higher the value. It is positive if the mouse
	 *   wheel is scrolled down and negative if it is scrolled up.
	 */
	var MouseScroll = 1 << 2;

	/**
	 * A `Focus` event is fired when the mouse clicks on a element that was not
	 * focused before. The previously focused element (might be `null` as well)
	 * is no longer focused then.
	 *
	 * **Possible states:**
	 *
	 *
	 * - `Activated`: Fired once when the element is focused
	 * - `Deactivated`: Fired once when the element is no longer focused. Also
	 *   fired when `EventHandler.clearFocus()` is called.
	 *
	 * **Further attributes:**
	 *
	 * - `mouseButton: MouseButton`: A `MouseButton` value describing
	 *   the mouse button that triggered this event. Focusing is still
	 *   button-independent!
	 */
	var Focus = 1 << 3;

	/**
	 * A `KeyCharPress` event is fired on the first frame a key is pressed and
	 * after that on a certain time interval to simulate a OS-like input
	 * behaviour. That interval can be controlled by
	 * `EventHandler.KEY_REPEAT_DELAY` and `EventHandler.KEY_REPEAT_PERIOD`.
	 *
	 * Please note that this event is only fired by keys that represent a
	 * character due to internal limitations. For an event that also handles
	 * other keys, please refer to [`KeyCodePress`](#KeyCodePress).
	 *
	 * **Possible states:**
	 *
	 * - `Active`
	 *
	 * **Further attributes:**
	 *
	 * - `keyChar: String`: The character of the key that triggered the event.
	 *
	 * @see [`KeyCodePress`](#KeyCodePress)
	 */
	var KeyCharPress = 1 << 4;

	/**
	 * A `KeyCodePress` event is fired on the first frame a key is pressed and
	 * after that on a certain time interval to simulate a OS-like input
	 * behaviour. That interval can be controlled by
	 * `EventHandler.KEY_REPEAT_DELAY` and `EventHandler.KEY_REPEAT_PERIOD`.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: Fired on the first frame a key is pressed down.
	 * - `Active`: Fired on all consecutive time steps of the interval.
	 *
	 * **Further attributes:**
	 *
	 * - `keyCode: kha.input.KeyCode`: The key code that corresponds to the key
	 *   that triggered the event.
	 *
	 * @see [`KeyCharPress`](#KeyCharPress)
	 */
	var KeyCodePress = 1 << 5;

	/**
	 * A `KeyCodeStatus` event is fired on the first frame a key was pressed down
	 * or was released, so everytime the key's **status** was changed.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: Fired on the first frame a key is pressed down.
	 * - `Deactivated`: Fired on the first frame after a key was released.
	 *
	 * **Further attributes:**
	 *
	 * - `keyCode: kha.input.KeyCode`: The key code that corresponds to the key
	 *   that triggered the event.
	 */
	var KeyCodeStatus = 1 << 6;

	/**
	 * Represents all event types. This value is used for event masks.
	 *
	 * @see `Element.eventMask`
	 */
	public static final ALL_TYPES = 0xffff;

	/**
	  * Represents no event types. This value is used for event masks.
	  *
	  * @see `Element.eventMask`
	  */
	public static final NO_TYPES = 0x0000;

	/**
	 * Represents only mouse event types. This value is used for event masks.
	 *
	 * @see `Element.eventMask`
	 */
	public static final MOUSE_TYPES = EventType.MouseHover | EventType.MouseClick | EventType.MouseScroll;

	/**
	  * Represents only keyboard event types. This value is used for event masks.
	  *
	  * @see `Element.eventMask`
	  */
	public static final KEY_TYPES = EventType.KeyCharPress | EventType.KeyCodePress | EventType.KeyCodeStatus;
}

/**
 * Represents the state of an event.
 *
 * > Not all states are possible for all events. A `EventType.MouseHover` event for
 * > example is never cancelled.
 *
 * @see [Wiki: Events/Event States](https://gitlab.com/koui/Koui/-/wikis/Documentation/Events#event-states)
 */
enum abstract EventState(Int) {
	/**
	 * Fired when the event happens for the first time after it's state was
	 * `Deactivated`.
	 */
	var Activated;

	/**
	 * Fired every frame as long as the interaction the event is describing is
	 * happening.
	 */
	var Active;

	/**
	 * When the event stopped **sucessfully** (it is not active anymore).
	 */
	var Deactivated;

	/**
	 * When the event stopped **unsuccessfully** (was cancelled). This happens
	 * for example if a `Button` element is held down and the mouse is released
	 * while no longer hovering the button.
	 */
	var Cancelled;
}

/**
 * Represents a mouse button.
 */
enum abstract MouseButton(Int) from Int to Int {
	var Left = 0;
	var Right = 1;
	var Middle = 2;
}
