package koui.events;

import haxe.ds.Vector;

import koui.Koui;
import koui.theme.Style;
import koui.elements.Element;
import koui.events.Events;
import koui.utils.Cursor;

/**
 * Responsible for the event handling of elements.
 */
class EventHandler {
	/**
	 * The x position of the mouse
	 */
	public static var mouseX(default, null) = 0;
	/**
	 * The y position of the mouse
	 */
	public static var mouseY(default, null) = 0;
	/**
	 * The delta x position of the mouse (the difference of the x position to
	 * the last frame)
	 */
	public static var mouseDX(default, null) = 0;
	/**
	 * The delta y position of the mouse (the difference of the y position to
	 * the last frame)
	 */
	public static var mouseDY(default, null) = 0;

	/**
	 * `true` if the `Ctrl` key is held down.
	 */
	public static var isCtrlDown(default, null) = false;
	/**
	 * `true` if the `Shift` key is held down.
	 */
	public static var isShiftDown(default, null) = false;
	/**
	 * `true` if the `Alt` key is held down.
	 */
	public static var isAltDown(default, null) = false;

	public static var mouse(default, null): Null<kha.input.Mouse>;
	public static var keyboard(default, null): Null<kha.input.Keyboard>;

	/**
	 * All currently active events.
	 */
	static var events: Map<Element, Array<Event>> = new Map<Element, Array<Event>>();

	static var mouseMoved = false;
	static var mousePressed: Vector<Bool> = new Vector<Bool>(3);
	static var lastPressedKey: Null<kha.input.KeyCode> = null;
	static var lastPressedKeyActive = false;
	static var lastPressedTime = 0.0;

	static var elemHovered: Null<Element> = null;
	static var elemFocused: Null<Element> = null;
	static var elemBlocking: Null<Element> = null;
	/**
	 * Unblock on the next click event
	 */
	static var markUnblock = false;
	/**
	 * `true` if the window is in the background and not focused.
	 */
	static var inBackground = false;

	public static function init() {
#if !(kha_android || kha_ios)
		mouse = kha.input.Mouse.get();
		mouse.notify(onMouseDown, onMouseUp, onMouseMove, onMouseScroll, onMouseLeave);
#end

		keyboard = kha.input.Keyboard.get();
		keyboard.notify(onKeyboardDown, onKeyboardUp, onKeyboardPress);

#if (kha_android || kha_ios)
		if (kha.input.Surface.get() != null) kha.input.Surface.get().notify(onTouchStart, onTouchEnd, onTouchMove);
#end

		kha.System.notifyOnApplicationState(onForeground, onResume, onPause, onBackground, onShutdown);
	}

	public static function registerElement(element: Element) {
		events[element] = new Array<Event>();
	}

	/**
	 * Adds an event to the given element. Used internally most of the time.
	 * @param element The element that is receiving the event. If `null`, no event is fired.
	 * @param event The event
	 */
	public static function addEvent(element: Null<Element>, event: Event) {
		if (element == null) return;
		event.element = element;
		events[element].push(event);
	}

	/**
	 * Cancels ALL events. Used internally most of the time.
	 */
	public static function reset() {
		for (element in events.keys()) {
			events[element] = new Array<Event>();
		}
	}

	/**
	 * Updates all the events and calls registered listeners. Used internally
	 * most of the time.
	 */
	public static function update() {
		if (inBackground) return;

		#if KOUI_MOUSE_ACCEL
		Cursor.calculateAccelMousePosition(mouseX, mouseY, mouseDX, mouseDY);
		#end

		// mouseDX = 0;
		// mouseDY = 0;

		if (mouseMoved) checkMouseHover();
		checkMousePressed();
		checkKeyCodePress();

		for (element in events.keys()) {
			if (elemBlocking != null && element != elemBlocking) continue;
			if (element.disabled) continue;

			for (event in events[element]) {
				switch (event.type) {
					case MouseHover:
						element._onHover(event);
						for (func in element.onMouseHoverFuncs) func(event);
					case MouseClick:
						element._onClick(event);
						for (func in element.onMouseClickFuncs) func(event);
					case MouseScroll:
						element._onScroll(event);
						for (func in element.onMouseScrollFuncs) func(event);
					case Focus:
						element._onFocus(event);
						for (func in element.onFocusFuncs) func(event);
					case KeyCharPress:
						element._onKeyCharPress(event);
						for (func in element.onKeyCharPressFuncs) func(event);
					case KeyCodePress:
						element._onKeyCodePress(event);
						for (func in element.onKeyCodePressFuncs) func(event);
					case KeyCodeStatus:
						element._onKeyCodeStatus(event);
						for (func in element.onKeyCodeStatusFuncs) func(event);
				}
			}
		}

		if (markUnblock) {
			elemBlocking = null;

			// Check again for hovering if unblocking when hovering over another
			// element
			checkMouseHover();

			// Set to false not before checkMouseHover() so that it works
			// correctly
			markUnblock = false;

		}
		mouseMoved = false;
	}

	@:allow(koui.elements.Element)
	static function clearFocus() {
		addEvent(elemFocused, {type: Focus, state: Deactivated, mouseButton: Left});
		elemFocused = null;
	}

	@:allow(koui.elements.Element)
	static function block(actuator: Element) {
		if (elemFocused != actuator) {
			trace('EventHandler warning: Cannot block, element $actuator is not focused! Focused: $elemFocused');
			return;
		}

		elemBlocking = actuator;
	}

	/**
	 * Unblocks the currently blocking element. The unblocking takes place in
	 * the next frame to not react to ongoing events. The `forceNow` parameter
	 * is a way around that behaviour.
	 *
	 * @param forceNow If `true`, already unblock in the current frame
	 */
	@:allow(koui.elements.Element)
	static function unblock(?forceNow = false) {
		if (forceNow) elemBlocking = null;

		else markUnblock = true;
	}

	public static inline function registerCutCopyPaste(onCut: Void -> String, onCopy: Void -> String, onPaste: String -> Void) {
		kha.System.notifyOnCutCopyPaste(onCut, onCopy, onPaste);
	}

	public static inline function unregisterCutCopyPaste() {
		kha.System.notifyOnCutCopyPaste(null, null, null);
	}

	static function onForeground() { inBackground = false; }
	static function onResume() {}
	static function onPause() {}
	static function onBackground() { inBackground = true; Cursor.setCursor(Default); }
	static function onShutdown() {}

	static function onMouseDown(button: Int, x: Int, y: Int) {
		addEvent(elemHovered, {type: MouseClick, state: Activated, mouseButton: button});

		if (elemFocused != elemHovered) {
			addEvent(elemFocused, {type: Focus, state: Deactivated, mouseButton: button});

			if (button == Left) {
				if (elemBlocking == null || elemBlocking == elemHovered) elemFocused = elemHovered;
				else elemFocused = null;

				addEvent(elemFocused, {type: Focus, state: Activated, mouseButton: button});
			}
			else elemFocused = null;
		}

		mousePressed[button] = true;
	}

	static function onMouseUp(button: Int, x: Int, y: Int) {
		if (elemFocused == elemHovered) {
			addEvent(elemFocused, {type: MouseClick, state: Deactivated, mouseButton: button});
		} else {
			addEvent(elemFocused, {type: MouseClick, state: Cancelled, mouseButton: button});
		}

		mousePressed[button] = false;
	}

	static function onMouseMove(x: Int, y: Int, deltaX: Int, deltaY: Int) {
		mouseX = x;
		mouseY = y;
		mouseDX = deltaX;
		mouseDY = deltaY;
		mouseMoved = true;

		#if !KOUI_MOUSE_ACCEL
		Cursor.setPosition(x, y);
		#end
	}

	static function onMouseScroll(scrollDelta: Int) {
		var receivingElement = getListeningParent(elemHovered, EventType.MouseScroll);
		addEvent(receivingElement, {type: MouseScroll, state: Active, scrollDelta: scrollDelta});
	}

	static function onMouseLeave() {}

	/**
	 * Called on the first frame a key is pressed down.
	 *
	 * @param key The keycode of the key
	 */
	static function onKeyboardDown(key: kha.input.KeyCode) {
		addEvent(elemFocused, {type: KeyCodeStatus, state: Activated, keyCode: key});

		// Simulate a similar behaviour to how "onKeyboardPress()" works
		lastPressedKey = key;
		lastPressedTime = kha.Scheduler.time() + Config.keyRepeatDelay;
		lastPressedKeyActive = true;

		if (key == Control) isCtrlDown = true;
		if (key == Shift) isShiftDown = true;
		if (key == Alt) isAltDown = true;
	}

	/**
	 * Called on the first frame a key is no longer pressed down.
	 *
	 * @param key The keycode of the key
	 */
	static function onKeyboardUp(key: kha.input.KeyCode) {
		addEvent(elemFocused, {type: KeyCodeStatus, state: Deactivated, keyCode: key});
		if (lastPressedKey == key) lastPressedKey = null;

		if (key == Control) isCtrlDown = false;
		if (key == Shift) isShiftDown = false;
		if (key == Alt) isAltDown = false;
	}

	/**
	 * Called when a key is pressed down. Note that this is called once on the
	 * first frame the key is pressed and after that on a certain time interval
	 * to simulate a OS-like input behaviour. Also, this method is only called
	 * by keys that represent a character.
	 *
	 * @param char The character that is pressed down
	 */
	static function onKeyboardPress(char: String) {
		addEvent(elemFocused, {type: KeyCharPress, state: Active, keyChar: char});
	}

#if (kha_android || kha_ios)
	static function onTouchStart(fingerID: Int, x: Int, y: Int) {
		mouseX = x;
		mouseY = y;
		checkMouseHover();
		onMouseDown(Left, x, y);
	}

	static function onTouchEnd(fingerID: Int, x: Int, y: Int) {
		onMouseUp(Left, x, y);
	}

	static function onTouchMove(fingerID: Int, x: Int, y: Int) {
		mouseX = x;
		mouseY = y;
		checkMouseHover();
	}
#end

	static function checkMouseHover() {
		var newElemHovered = getListeningParent(Koui.getElementAtPosition(mouseX, mouseY), EventType.MouseHover);

		if (newElemHovered != elemHovered || markUnblock) {
			addEvent(elemHovered, {type: MouseHover, state: Deactivated, mouseMoved: true});
			addEvent(newElemHovered, {type: MouseHover, state: Activated, mouseMoved: true});

			if (elemBlocking == null || elemBlocking == newElemHovered) elemHovered = newElemHovered;
			else elemHovered = null;

			if (elemHovered != null) {
				if (elemHovered.disabled) Cursor.setCursor(Cursor.getCursorEnumValue(Style.getStyle(elemHovered.tID).cursors.notallowed));
				else Cursor.setCursor(Cursor.getCursorEnumValue(Style.getStyle(elemHovered.tID).cursors.defaultCursor));
			} else {
				if (elemBlocking == null) Cursor.setCursor(Default);
			}
		}
		else addEvent(newElemHovered, {type: MouseHover, state: Active, mouseMoved: mouseMoved});
	}

	static function checkMousePressed() {
		for (button in 0...mousePressed.length) {
			if (mousePressed[button]) {
				addEvent(elemFocused, {type: MouseClick, state: Active, mouseButton: button});
			}
		}
	}

	public static function checkKeyCodePress() {
		if (lastPressedKey == null) return;

		if (lastPressedKeyActive) {
			addEvent(elemFocused, {type: KeyCodePress, state: Activated, keyCode: lastPressedKey});
			lastPressedKeyActive = false;
		}
		else if (kha.Scheduler.time() - lastPressedTime > Config.keyRepeatPeriod) {
			addEvent(elemFocused, {type: KeyCodePress, state: Active, keyCode: lastPressedKey});
			lastPressedTime = kha.Scheduler.time();
		}
	}

	/**
	 * Return the lowest element in the parent/layout hierarchy (beginning with
	 * and including the passed element) that listens to the given event type.
	 */
	static function getListeningParent(element: Null<Element>, eventType: EventType): Null<Element> {
		if (element == null) {
			return null;
		}

		var currentElement = element;

		do {
			if ((currentElement.eventMask & eventType) != 0) {
				return currentElement;
			}
			if (currentElement.layout == null) {
				break;
			}
			currentElement = currentElement.layout;
		} while (currentElement.layout != null);

		return null;
	}
}
