package koui.elements;

import koui.utils.RadioGroup;
import koui.utils.TextureAtlas;
import koui.theme.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A radio button is a special kind of checkbox that is used to select exactly
 * one option from a set of options. Radio buttons belong to a `RadioGroup` that
 * holds all related radio buttons from which only one option can be selected.
 *
 * ![RadioButton screenshot](https://gitlab.com/koui/Koui/-/wikis/images/elements/element_radiobutton.png)
 *
 * @see `RadioGroup`
 * @see `Checkbox`
 */
class RadioButton extends Checkbox {
	/**
	 * The `RadioGroup` this radio button belongs to.
	 */
	public var group: RadioGroup;

	/**
	 * Creates a new `RadioButton`.
	 * @param group The `RadioGroup` this radio button belongs to.
	 * @param label The label of this radio button.
	 */
	public function new(group: RadioGroup, label: String) {
		super(label);

		requireStates(["hover", "click"]);
		requireStates(["hover", "click"], "inner");
		requireStates(["hover", "click"], "inner_checked");

		this.group = group;
		this.group.add(this);
	}

	#if !KOUI_EVENTS_OFF
	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated: isClicked = true;
			case Active:
			case Deactivated:
				isClicked = false;
				if (!isChecked) {
					group.setActiveButton(this);
				}
			case Cancelled:
				isClicked = false;
		}
	}
	#end

	public override function draw(g: koui.graphics.KGraphics) {
		if (isClicked) setContextState("click");
		else if (isHovered) setContextState("hover");

		if (style.textureBg != "") {
			paddingY = Std.int((drawHeight - style.atlas.h) / 2);
		}

		var atlasOffsetX = isChecked ? 1 : 0;
		var atlasOffsetY = isClicked ? 2 : (isHovered ? 1 : 0);

		if (!TextureAtlas.drawFromAtlas(g, this, 2, 3, atlasOffsetX, atlasOffsetY, drawX + paddingX, drawY + paddingY)) {
			// =================================================================
			// Background
			// =================================================================
			g.fillKRect(drawX, drawY, drawWidth, drawHeight, style);

			// =================================================================
			// Foreground/Checkbox
			// =================================================================
			if (isChecked) setContextElement("inner_checked");
			else setContextElement("inner");

			g.fillKCircle(drawX + paddingX + checkSize / 2, drawY + drawHeight / 2, checkSize / 2, style);
		}

		var offsetLeft = paddingX * 2 + checkSize;
		g.drawKAlignedString(label, drawX + offsetLeft, drawY + drawHeight / 2, TextLeft, TextMiddle, style);
	}
}
