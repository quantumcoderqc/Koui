package koui.elements;

import haxe.ds.Vector;

import koui.graphics.KGraphics;
import koui.elements.layouts.Layout;
import koui.elements.layouts.ScrollPane;
import koui.events.EventHandler;
import koui.theme.Style;
import koui.utils.MathUtil;
import koui.utils.Set;

/**
 * The base class of all elements.
 */
@:allow(koui.Koui)
@:allow(koui.events.EventHandler)
@:autoBuild(koui.theme.ThemeUtil.buildSubElement())
class Element {
	/**
	 * Set of elements for which to notify their parent layout about a change of
	 * position or size on the next frame.
	 */
	static var invalidations: Set<Element> = new Set();

	/** The x position of this element. */
	public var posX(default, set): Int;
	/** The y position of this element. */
	public var posY(default, set): Int;
	/** The width of this element. */
	public var width(default, set): Int;
	/** The height of this element. */
	public var height(default, set): Int;

	/**
	 * The anchor of this element. Used for the layout.
	 * @see `Anchor`
	 */
	public var anchor = Anchor.FollowLayout;

	/**
	 * `true` if the element is disabled and should not react to events. Also,
	 * elements might look different if disabled depending on the theme.
	 */
	public var disabled = false;

	/**
	 * If `false`, the element is not visible and will not react to any events.
	 */
	public var visible = true;

	/**
	 * This bit mask specifies which event types this element should listen to.
	 * If the element does receive an event but does not listen to it, it's
	 * parent element will receive the event instead (if that doesn't listen to
	 * the event type, it will go on recursively).
	 *
	 * **Per default, all elements that usually don't require scrolling do not
	 * listen to scroll events unless explicitly enabled.**
	 *
	 * @see `EventType`
	 */
	public var eventMask: Int = EventType.ALL_TYPES & ~EventType.MouseScroll;

	/**
	 * The layout this element belongs to.
	 */
	public var layout(default, null): Layout = null;

	/**
	 * The theme ID of this object. Use this to select which theme settings to
	 * apply.
	 */
	var tID(default, set) = "_root";

	/** ctxElement!state => style */
	var stylesCache(default, null): Map<String, Style>;
	/** Style of the current context */
	var style(default, null): Style;
	/** States for each context element: ctxElement => states */
	var states(default, null) = ["" => ["default", "disabled"]];
	/** Current context state */
	var state(default, null) = "default";
	/** Current context element */
	var ctxElement(default, null) = "";

	/** The x position of this element, used for drawing and event handling. */
	var drawX(default, null): Int;
	/** The y position of this element, used for drawing and event handling. */
	var drawY(default, null): Int;
	/** The width of this element, used for drawing and event handling. */
	var drawWidth(default, null): Int;
	/** The height of this element, used for drawing and event handling. */
	var drawHeight(default, null): Int;
	/** The x position of this element, used for the layout. */
	var layoutX(default, set): Int;
	/** The y position of this element, used for the layout. */
	var layoutY(default, set): Int;
	/** The width of this element, used for the layout. */
	var layoutWidth(default, set): Int;
	/** The height of this element, used for the layout. */
	var layoutHeight(default, set): Int;

	var onMouseHoverFuncs: Array<Event -> Void> = new Array();
	var onMouseClickFuncs: Array<Event -> Void> = new Array();
	var onMouseScrollFuncs: Array<Event -> Void> = new Array();
	var onFocusFuncs: Array<Event -> Void> = new Array();
	var onKeyCharPressFuncs: Array<Event -> Void> = new Array();
	var onKeyCodePressFuncs: Array<Event -> Void> = new Array();
	var onKeyCodeStatusFuncs: Array<Event -> Void> = new Array();

	/**
	 * Create a new `Element`.
	 */
	function new(posX: Int = 0, posY: Int = 0, width: Int = 0, height: Int = 0, ?__isSuperCall = false) {
		this.posX = this.layoutX = posX;
		this.posY = this.layoutY = posY;
		this.width = this.layoutWidth = width;
		this.height = this.layoutHeight = height;

		EventHandler.registerElement(this);
	}

	/**
	 * Return a string representation of this element:
	 * `"Element: <[ClassName]>"`
	 */
	public function toString(): String {
		return 'Element: <${Type.getClassName(Type.getClass(this))}>';
	}

	/**
	 * Setup the given element for drawing and call its draw method afterwards.
	 */
	public final inline function renderElement(g: KGraphics, element: Element) {
		if (!element.visible) return;
		g.opacity = element.style.opacity;

		element.resetContext();
		if (element.disabled) element.setContextState("disabled");

		element.draw(g);
	}

	function draw(g: KGraphics) {
		Log.error("draw() function must be overriden by element!");
	}

	function drawOverlay(g: KGraphics) {}

// =============================================================================
// POSITION & SIZE
// =============================================================================

	/**
	 * Set the position of this element. You can also change the position
	 * per axis, see [`posX`](#posX) and [`posY`](#posY).
	 */
	public inline function setPosition(posX: Int, posY: Int) {
		this.posX = posX;
		this.posY = posY;
	}

	/**
	 * Set the size of this element. You can also change the size for each
	 * individual side, see [`width`](#width) and [`height`](#height).
	 */
	public inline function setSize(width: Int, height: Int) {
		this.width = width;
		this.height = height;
	}

	/**
	 * Return the offset of the element's position to the screen.
	 */
	public function getLayoutOffset(): Vector<Int> {
		var offset = new Vector<Int>(2);
		offset[0] = 0;
		offset[1] = 0;

		// Start with `this` to be less redundant with the following code
		var currentParent: Element = this;
		while (currentParent.layout != null) {
			currentParent = currentParent.layout;

			if (Std.is(currentParent, Layout)) {
				offset[0] -= currentParent.layoutX;
				offset[1] -= currentParent.layoutY;

				if (Std.is(currentParent, ScrollPane)) {
					var sp = cast(currentParent, ScrollPane);
					offset[0] += Std.int(@:privateAccess sp.scrollX);
					offset[1] += Std.int(@:privateAccess sp.scrollY);
				}
			}
		}

		return offset;
	}

	/**
	 * Return `true` if this element overlaps with the given position. Used
	 * internally for event handling most of the time. Elements may override
	 * this method to provide more detailed mouse interaction.
	 * If the element is invisible, `false` is returned.
	 */
	public inline function isAtPosition(x: Int, y: Int) {
		if (!visible) return false;
		// Use drawing measures to take overlays into account
		return MathUtil.hitbox(x, y, this.drawX, this.drawY, this.drawWidth, this.drawHeight);
	}

	inline function getAnchorResolved(): Anchor {
		if (this.anchor == FollowLayout) return this.layout.defaultAnchor;
		return this.anchor;
	}

	/**
	 * Notify the parent layout on the next frame that this element has changed
	 * its size or position. Use the next frame to make sure we don't waste
	 * calculations when more than one value changes during one frame.
	 */
	inline function invalidateElem() {
		Element.invalidations.add(this);
	}

	function set_posX(value: Int) { if (value != posX) { invalidateElem(); } return posX = layoutX = value; }
	function set_posY(value: Int) { if (value != posY) { invalidateElem(); } return posY = layoutY = value; }
	function set_width(value: Int) { if (value != width) { invalidateElem(); } return width = layoutWidth = value; }
	function set_height(value: Int) { if (value != height) { invalidateElem(); } return height = layoutHeight = value; }
	function set_layoutX(value: Int) { return layoutX = drawX = value; }
	function set_layoutY(value: Int) { return layoutY = drawY = value; }
	function set_layoutWidth(value: Int) { return layoutWidth = drawWidth = value; }
	function set_layoutHeight(value: Int) { return layoutHeight = drawHeight = value; }

// =============================================================================
// STYLE & DRAWING CONTEXT
// =============================================================================

	/**
	 * `tID` setter. Caches all styles required by this element.
	 */
	function set_tID(value: String): String {
		this.tID = value;

		this.stylesCache = new Map();

		for (ctxElementName => ctxElementStates in this.states) {
			// Get selector name from suffix
			var selectorBaseName = (ctxElementName == "") ? tID : tID + "_" + ctxElementName;

			for (s in ctxElementStates) {
				// Use the context element name as the key to make the cache
				// independent of the tID.
				var key = ctxElementName + "!" + s;

				stylesCache[key] = Style.getStyle(selectorBaseName, s);
				if (stylesCache[key] == null) {
					Log.error('Missing selector in theme file: "${selectorBaseName + "!" + s}"!');
				}
			}
		}

		loadStyle();

		return tID;
	}

	@:dox(hide)
	function initTID(tID: String, constrCalled: String) {
		this.tID = tID;
		onTIDChange(constrCalled);
	}

	/**
	 * Set the theme ID of this element.
	 */
	public function setTID(tID: String) {
		if (this.tID != tID) {
			this.tID = tID;
			setContextElement("");
			onTIDChange();
		}
	}
	public inline function getTID(): String { return this.tID; }

	inline function requireStates(states: Array<String>, ctxElement: String = "") {
		requireContextElement(ctxElement);
		this.states[ctxElement] = this.states[ctxElement].concat(states);
	}

	inline function requireContextElement(ctxElement: String) {
		if (!this.states.exists(ctxElement)) {
			this.states[ctxElement] = ["default", "disabled"];
		}
	}

	/**
	 * Set the current style context used for drawing. Calling this function is
	 * equivalent to calling `setContextState` and `setContextElement`. If the
	 * given element name is `""`, the element's `tID` is used.
	 *
	 * @param state
	 * @param ctxElement
	 *
	 * @see `setContextElement`
	 * @see `setContextState`
	 * @see `resetContext`
	 */
	inline function setContext(state: String, ctxElement: String = "") {
		this.state = state;
		this.ctxElement = ctxElement;
		loadStyle();
	}

	/**
	 * Set the drawing context element but keep the current state. If the given
	 * element name is `""`, the element's `tID` is used.
	 *
	 * @see `setContextState`
	 * @see `setContext`
	 * @see `resetContext`
	 */
	inline function setContextElement(ctxElement: String = "") {
		this.ctxElement = ctxElement;
		loadStyle();
	}

	/**
	 * Set the drawing context state but keep the current element.
	 *
	 * @see `setContextElement`
	 * @see `setContext`
	 * @see `resetContext`
	 */
	inline function setContextState(state: String) {
		this.state = state;
		loadStyle();
	}

	function loadStyle() {
		var stateStyle = stylesCache[ctxElement + "!" + state];

		if (stateStyle != null) {
			this.style = stateStyle;
		}
		else {
			if (ctxElement == "") ctxElement = tID;
			else ctxElement = tID + "_" + ctxElement;
			Log.error('No cached style found for tID "$ctxElement" and state "$state"!');
		}
	}

	inline function resetContext() {
		this.setContext("default", "");
	}

	/**
	 * Overriding methods must keep the same parameter name for internal
	 * reasons. If the parameter has another name, an exception is thrown.
	 *
	 * @param constrCalled The name of the calling class if this was called from
	 *                     a constructor
	 */
	function onTIDChange(?constrCalled: String) {}

// =============================================================================
// EVENTS
// =============================================================================

	/**
	 * Call the given function when a `MouseHover` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onMouseHover(func: Event -> Void) {
		this.onMouseHoverFuncs.push(func);
	}

	/**
	 * Call the given function when a `MouseClick` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onMouseClick(func: Event -> Void) {
		this.onMouseClickFuncs.push(func);
	}

	/**
	 * Call the given function when a `MouseScroll` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onMouseScroll(func: Event -> Void) {
		this.onMouseScrollFuncs.push(func);
	}

	/**
	 * Call the given function when a `Focus` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onFocus(func: Event -> Void) {
		this.onFocusFuncs.push(func);
	}

	/**
	 * Call the given function when a `KeyPress` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onKeyCharPress(func: Event -> Void) {
		this.onKeyCharPressFuncs.push(func);
	}

	/**
	 * Call the given function when a `KeyTyped` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onKeyCodePress(func: Event -> Void) {
		this.onKeyCodePressFuncs.push(func);
	}

	/**
	 * Call the given function when a `KeyCode` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onKeyCodeStatus(func: Event -> Void) {
		this.onKeyCodeStatusFuncs.push(func);
	}

	// Internal use only
	function _onHover(event: Event) {}
	function _onClick(event: Event) {}
	function _onScroll(event: Event) {}
	function _onKeyCharPress(event: Event) {}
	function _onKeyCodePress(event: Event) {}
	function _onKeyCodeStatus(event: Event) {}
	function _onFocus(event: Event) {}
}
