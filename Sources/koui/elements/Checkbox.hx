package koui.elements;

import koui.utils.TextureAtlas;
import koui.theme.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A checkbox is a button that represents an option, it can either be switched
 * on or off. It has a text label that is describing the function of the
 * checkbox.
 *
 * ![Checkbox screenshot](https://gitlab.com/koui/Koui/-/wikis/images/elements/element_checkbox.png)
 *
 * ```haxe
 * // Construct a new Checkbox with the label "Checkbox"
 * var myCheckbox = new Checkbox("Checkbox");
 * // Make the checkbox checked (select it)
 * myCheckbox.isChecked = true;
 * ```
 *
 * @see `RadioButton`
 */
class Checkbox extends Element {
	/**
	 * The label of this checkbox.
	 */
	public var label = "";
	/**
	 * `true` when this checkbox is active, otherwise `false`.
	 */
	public var isChecked = false;

	var onCheckedFunc: Void->Void = () -> {};
	var onUncheckedFunc: Void->Void = () -> {};

	var paddingX: Int = 0;
	var paddingY: Int = 0;

	var checkSize: Int;

	var isHovered = false;
	var isClicked = false;

	/**
	 * Create a new `Checkbox` element.
	 */
	public function new(label: String) {
		super();

		requireStates(["hover", "click"]);
		requireStates(["hover", "click"], "inner");
		requireStates(["hover", "click"], "inner_checked");

		this.label = label;
	}

	override function onTIDChange(?constrCalled: String) {
		ThemeUtil.requireOptGroups(["checkbox"]);

		width = style.size.width;
		height = style.size.height;

		setContextElement("inner");

		checkSize = style.size.height;
		paddingY = Std.int((height - checkSize) / 2);

		setContextElement("");

		paddingX = style.checkbox.useAutoPadding ? paddingY : style.padding.left;
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated: isHovered = true;
			case Active:
			case Deactivated, Cancelled: isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated: isClicked = true;
			case Active:
			case Deactivated:
				isClicked = false;
				isChecked = !isChecked;
			case Cancelled:
				isClicked = false;
		}
	}
	#end

	override public function draw(g: koui.graphics.KGraphics) {
		if (isClicked) setContextState("click");
		else if (isHovered) setContextState("hover");

		if (style.textureBg != "") {
			paddingY = Std.int((drawHeight - style.atlas.h) / 2);
		}

		var atlasOffsetX = isChecked ? 1 : 0;
		var atlasOffsetY = isClicked ? 2 : (isHovered ? 1 : 0);

		if (!TextureAtlas.drawFromAtlas(g, this, 2, 3, atlasOffsetX, atlasOffsetY, drawX + paddingX, drawY + paddingY)) {
			// =================================================================
			// Background
			// =================================================================
			g.fillKRect(drawX, drawY, drawWidth, drawHeight, style);

			// =================================================================
			// Foreground/Checkbox
			// =================================================================
			if (isChecked) setContextElement("inner_checked");
			else setContextElement("inner");

			g.fillKRect(drawX + paddingX, drawY + (drawHeight - checkSize) / 2, checkSize, checkSize, style);
			setContextElement("");
		}

		var offsetLeft = paddingX * 2 + checkSize;
		g.drawKAlignedString(label, drawX + offsetLeft, drawY + drawHeight / 2, TextLeft, TextMiddle, style);
	}

	/**
	 * Run the given callback when the checkbox was checked.
	 */
	public inline function onChecked(callback: Void->Void) {
		onCheckedFunc = callback;
	}

	/**
	 * Run the given callback when the checkbox was unchecked.
	 */
	public inline function onUnchecked(callback: Void->Void) {
		onUncheckedFunc = callback;
	}
}
