package koui.elements;

import koui.utils.TextureAtlas;
import koui.theme.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A button is an element that performs an action when clicked. It has a text
 * label that is describing the function of the button.
 *
 * ![Button screenshot](https://gitlab.com/koui/Koui/-/wikis/images/elements/element_button.png)
 *
 * ```haxe
 * // Construct a new Button with the label "Ok"
 * var myButton = new Button("Ok");
 *
 * myButton.onMouseClick((event: Event) -> {
 *     // If the left mouse button is released over the button, quit the application
 *     if (event.mouseButton == Left && event.state == Deactivated) {
 *          kha.System.stop();
 *     }
 * });
 * ```
 */
class Button extends Element {
	/**
	 * The label of this button.
	 */
	public var label = "";

	var isHovered = false;
	var isClicked = false;

	/**
	 * Create a new `Button` element.
	 */
	public function new(label: String) {
		super();

		this.label = label;
		requireStates(["hover", "click"]);
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated: isHovered = true;
			case Active:
			case Deactivated, Cancelled: isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated: isClicked = true;
			case Active:
			case Deactivated, Cancelled: isClicked = false;
		}
	}
	#end

	override function onTIDChange(?constrCalled: String) {
		if (style.textureBg == "") {
			width = style.size.width;
			height = style.size.height;
		} else {
			width = style.atlas.w;
			height = style.atlas.h;
		}
	}

	override public function draw(g: koui.graphics.KGraphics) {
		var atlasOffsetY = isClicked ? 2 : (isHovered ? 1 : 0);

		if (!TextureAtlas.drawFromAtlas(g, this, 1, 3, 0, atlasOffsetY)) {
			if (isClicked) setContextState("click");
			else if (isHovered) setContextState("hover");

			g.fillKRect(drawX, drawY, drawWidth, drawHeight, style);
		}

		g.drawKAlignedString(label, drawX + drawWidth / 2, drawY + drawHeight / 2, TextCenter, TextMiddle, style);
	}

	override public function toString(): String {
		return 'Element: <${Type.getClassName(Type.getClass(this))}>{Label: "${this.label}"}';
	}
}
