package koui.elements;

import kha.FastFloat;

import koui.events.EventHandler;
import koui.utils.MathUtil;
import koui.theme.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A slider is an element for controlling a value inside a certain range.
 * Sliders also have an attribute called [`precision`](#precision) that defines
 * the number of decimal places the value can have.
 *
 * ![Slider screenshot](https://gitlab.com/koui/Koui/-/wikis/images/elements/element_slider.png)
 *
 * ```haxe
 * // Construct a new Slider with values between 0 and 100
 * var mySlider = new Slider(0, 100);
 *
 * // Accept two decimal places...
 * mySlider.precision = 2:
 *
 * // ...or only accept whole numbers
 * mySlider.precision = 0;
 *
 * // Set the current value to 32
 * mySlider.value = 32;
 * ```
 *
 * @see [Wiki: Slider](https://gitlab.com/koui/Koui/-/wikis/Documentation/Elements/Slider)
 */
class Slider extends Element {
	/**
	 * The current value of this element.
	 */
	public var value: FastFloat = 0.0;

	/**
	 * The number of decimal places the value of this element has.
	 */
	public var precision = 0;

	/**
	 * The direction this slider is oriented to.
	 */
	public var orientation: SliderOrientation;

	/**
	 * The minimum value of this slider. Must not be bigger than
	 * `Slider.maxValue`.
	 */
	public var minValue(default, set): FastFloat = 0.0;
	function set_minValue(value: FastFloat) {
		this.minValue = value;
		validateMinMax();
		return this.minValue;
	}

	/**
	 * The maximum value of this slider. Must not be bigger than
	 * `Slider.minValue`.
	 */
	public var maxValue(default, set): FastFloat = 1.0;
	function set_maxValue(value: FastFloat) {
		this.maxValue = value;
		validateMinMax();
		return this.maxValue;
	}

	var isHovered = false;
	var isClicked = false;
	var clickOffset = 0;

	var buttonWidth = 0;

	var onValueChangedFunc: FastFloat->Void = (newValue: FastFloat) -> {};

	/**
	 * Creates a new `Slider` with the given range.
	 *
	 * @param minValue The lower bound of the slider's range
	 * @param maxValue The upper bound of the slider's range
	 * @param orientation Override the orientation
	 */
	public function new(minValue: FastFloat, maxValue: FastFloat, orientation = SliderOrientation.Right) {
		super();

		requireStates(["hover", "click"]);
		requireStates(["hover", "click"], "inner");

		this.orientation = orientation;
		this.minValue = minValue;
		this.maxValue = maxValue;

		if (maxValue - minValue < 10) precision = 1;
	}

	override function onTIDChange(?constrCalled: String) {
		if (style.textureBg == "") {
			// Don't require an extra vertical style from the user,
			// just switch dimensions instead
			width = isHorizontal() ? style.size.width : style.size.height;
			height = isHorizontal() ? style.size.height : style.size.width;
		}
		else {
			width = isHorizontal() ? style.atlas.w : style.atlas.h;
			height = isHorizontal() ? style.atlas.h : style.atlas.w;
		}

		if (style.slider.buttonTexture == null) {
			buttonWidth = style.slider.buttonWidth;
		} else {
			buttonWidth = style.slider.buttonTexture.atlasW;
		}
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated: isHovered = true;
			case Active:
			case Deactivated, Cancelled: isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated:
				isClicked = true;
				clickOffset = isHorizontal() ? this.getLayoutOffset()[0] : this.getLayoutOffset()[1];
				EventHandler.block(this);

			case Active:
				var oldValue = value;

				if (isHorizontal()) {
					value = MathUtil.mapToRange(
						EventHandler.mouseX + clickOffset,
						drawX + buttonWidth / 2, drawX + drawWidth - buttonWidth / 2,
						minValue, maxValue);

					if (orientation == Left) {
						value = maxValue - value;
					}
				}
				else {
					value = MathUtil.mapToRange(
						EventHandler.mouseY + clickOffset,
						drawY + buttonWidth / 2, drawY + drawHeight - buttonWidth / 2,
						minValue, maxValue);

					if (orientation == Up) {
						value = maxValue - value;
					}
				}

				value = MathUtil.clamp(value, minValue, maxValue);

				if (oldValue != value) {
					onValueChangedFunc(value);
				}

			case Deactivated, Cancelled:
				isClicked = false;
				EventHandler.unblock();
		}
	}
	#end

	override function draw(g: koui.graphics.KGraphics) {
		var horizontal = isHorizontal();

		// =====================================================================
		// Background
		// =====================================================================
		var imageName = style.textureBg;
		if (imageName == "") {
			if (isClicked) setContextState("click");
			else if (isHovered) setContextState("hover");

			g.fillKRect(drawX, drawY, drawWidth, drawHeight, style);
		}

		else {
			var atlasX: Int = style.atlas.x;
			var atlasY: Int = style.atlas.y;
			var atlasW: Int = style.atlas.w;
			var atlasH: Int = style.atlas.h;

			width = atlasW;
			height = atlasH;
			drawWidth = atlasW;
			drawHeight = atlasH;

			g.color = 0xffffffff;
			g.drawSubImage(kha.Assets.images.get(imageName), drawX, drawY, atlasX, atlasY, atlasW, atlasH);
			setContextState("default");
		}

		// =====================================================================
		// Thumb
		// =====================================================================
		setContextElement("inner");
		if (style.slider == null || style.slider.buttonTexture == null) {
			if (isClicked) setContextState("click");
			else if (isHovered) setContextState("hover");

			if (horizontal) {
				g.fillKRect(calculateSliderPosition(), drawY, buttonWidth, drawHeight, style);
			}
			else {
				g.fillKRect(drawX, calculateSliderPosition(), drawWidth, buttonWidth, style);
			}
		}

		else {
			var atlasX: Int = style.slider.buttonTexture.atlasX;
			var atlasY: Int = style.slider.buttonTexture.atlasY;
			var atlasW: Int = style.slider.buttonTexture.atlasW;
			var atlasH: Int = style.slider.buttonTexture.atlasH;

			if (isClicked) atlasY += atlasH * 2;
			else if (isHovered) atlasY += atlasH;

			g.color = 0xffffffff;
			if (horizontal) {
				g.drawSubImage(kha.Assets.images.get(style.slider.buttonTexture.image), calculateSliderPosition(), drawY, atlasX, atlasY, atlasW, atlasH);
			}
			else {
				g.drawSubImage(kha.Assets.images.get(style.slider.buttonTexture.image), drawY, calculateSliderPosition(), atlasX, atlasY, atlasW, atlasH);
			}
		}

		// =====================================================================
		// Text
		// =====================================================================
		setContextElement("");

		g.color = style.color.text;
		g.font = ThemeUtil.getFont();
		g.fontSize = style.font.size;

		var textPosY = Std.int(drawY + drawHeight / 2);

		var tOffsetX = style.slider.textOffsetX;
		var tOffsetY = style.slider.textOffsetY;

		if (style.slider.showRange) {
			if (horizontal) {
				var textLeft = orientation == Right ? "" + minValue : "" + maxValue;
				var textRight = orientation == Right ? "" + maxValue : "" + minValue;
				g.drawKAlignedString(textLeft, drawX + tOffsetX, textPosY + tOffsetY, TextLeft, TextMiddle, style);
				g.drawKAlignedString(textRight, drawX + drawWidth + tOffsetX, textPosY + tOffsetY, TextRight, TextMiddle, style);
			}
			else {
				var textTop = orientation == Down ? "" + minValue : "" + maxValue;
				var textBottom = orientation == Down ? "" + maxValue : "" + minValue;
				g.drawKAlignedString(textTop, drawX + drawWidth / 2 + tOffsetX, drawY + tOffsetY, TextCenter, TextTop, style);
				g.drawKAlignedString(textBottom, drawX + drawWidth / 2 + tOffsetX, drawY + drawHeight + tOffsetY, TextCenter, TextBottom, style);
			}
		}

		if (style.slider.showValue) {
			if (horizontal) {
				g.drawKAlignedString("" + MathUtil.roundPrecision(value, this.precision), drawX + drawWidth / 2 + tOffsetX, textPosY + tOffsetY, TextCenter, TextMiddle, style);
			}
			else {
				g.drawKAlignedString("" + MathUtil.roundPrecision(value, this.precision), drawX + drawWidth / 2 + tOffsetX, drawY + drawHeight / 2 + tOffsetY, TextCenter, TextMiddle, style);
			}
		}
	}

	function calculateSliderPosition(): FastFloat {
		return switch (orientation) {
			case Up:
				MathUtil.mapToRange(value, minValue, maxValue, drawY + drawHeight - buttonWidth, drawY);
			case Down:
				MathUtil.mapToRange(value, minValue, maxValue, drawY, drawY + drawHeight - buttonWidth);
			case Right:
				MathUtil.mapToRange(value, minValue, maxValue, drawX, drawX + drawWidth - buttonWidth);
			case Left:
				MathUtil.mapToRange(value, minValue, maxValue, drawX + drawWidth - buttonWidth, drawX);
		}
	}

	/**
	 * Quickly check if this slider is oriented horizontally (left or right).
	 */
	public inline function isHorizontal() {
		return orientation == Left || orientation == Right;
	}

	inline function validateMinMax() {
		if (minValue > maxValue) {
			Log.error("Slider: minValue must not be larger than maxValue!");
		}
	}

	/**
	 * Run the given callback when the value of the slider was changed.
	 */
	public inline function onValueChanged(callback: FastFloat->Void) {
		onValueChangedFunc = callback;
	}
}

enum abstract SliderOrientation(Int) {
	var Up;
	var Down;
	var Left;
	var Right;
}
