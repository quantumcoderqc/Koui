package koui.utils;

/**
 * Logging utility.
 */
class Log {
	/**
	 * Returns an error message in the format `"[Koui Error] <message>"`.
	 */
	public static inline function error(message: String) {
		throw "[Koui Error] " + message;
	}

	/**
	 * Outputs a warning in the format `"[Koui Warning] <message>`.
	 */
	public static inline function warn(message: String) {
		throw "[Koui Warning] " + message;
	}

	#if macro
	public static inline function out(message: String) {
		Sys.println("[Koui] " + message);
	}
	#end
}
